FROM openjdk
MAINTAINER TAYARI Oussama
WORKDIR /home
COPY target/springboot-kubernetes-0.0.1-SNAPSHOT.jar  myappspring.jar
ENTRYPOINT ["java","-jar","myappspring.jar"]
